package org.xxx.bitbucket.impl;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The only purpose of this component is to let bitbucket-maven-plugin to fill content of META-INF/plugin-components/imports file.
 * It adds a line for every @ComponentImport line in the code
 */
@Component
public class ComponentsImporter {

    @ComponentImport
    private AuthenticationContext authenticationContext;

    @Autowired
    public void setAuthenticationContext(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

}
